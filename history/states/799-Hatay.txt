state={
	id=799
	name="STATE_799"
	provinces={
		12029 
	}
	manpower = 234379
	
	state_category = enclave

	history= {
		owner = TST
		add_core_of = TST
		add_core_of = TST
		victory_points = {
			12029 1
		}
		buildings = {
			infrastructure = 1
		}

		1938.9.30 = {
			owner = TST
			controller = TST
		}
	}

	local_supplies=0.0 
}
