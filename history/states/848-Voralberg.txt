
state={
	id=848
	name="STATE_848"
	manpower = 154354
	state_category = pastoral
	history={
		owner = TST
		buildings = {
			infrastructure = 2
			industrial_complex = 1
		}
		add_core_of = TST
		add_claim_by = TST
		1938.3.12 = {
			owner = TST
			controller = TST
			add_core_of = TST
		}
	}
	provinces={
		6678 6680 
	}
	buildings_max_level_factor=1.000
	local_supplies=0.000
}
