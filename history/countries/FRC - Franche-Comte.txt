﻿capital = 14

set_research_slots = 1
set_stability = 0.6
set_war_support = 0.4  

set_technology = {
}

set_convoys = 50

set_politics = {
	ruling_party = despotist
	elections_allowed = no
}

set_popularities = {
	despotist = 90
	absolutist = 10
}