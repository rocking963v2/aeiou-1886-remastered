﻿capital = 60

set_research_slots = 1

set_stability = 0.6
set_war_support = 0.5

# Starting tech
set_technology = {

}

add_ideas = {
	hre_idea_prince
}

set_convoys = 800

# DIPLOMACY

set_politics = {
	ruling_party = absolutist
	elections_allowed = no
}
set_popularities = {
	absolutist = 100
}

create_country_leader = {
	name = "Karl Alexander I"
	desc = "POLITICS_ALBERT_SAXONY"
	picture = "Portrait_THU_Karl_Alexander.dds"
	expire = "1902.6.19"
	ideology = absolutist_moderate
	traits = {
		#traits here need to also be added to events britain.9 and britain.10. Don't ask why.
	}
}

add_to_array = { national_religion_array =  2 }
add_to_array = { national_religion_array =  3 }
