﻿capital = 51

set_research_slots = 1

set_technology = {
}

add_ideas = {
	hre_idea_prince
}

set_politics = {
	ruling_party = absolutist
	elections_allowed = no
}
set_popularities = {
	absolutist = 100
}
add_to_array = { national_religion_array =  2 }

create_country_leader = {
	name = "Karl Egon IV"
	desc = "POLITICS_KARL_EGON_DESC"
	picture = "Portrait_JUL_Karl_Egon_IV.dds"
	expire = "1965.1.1"
	ideology = absolutist_moderate
}